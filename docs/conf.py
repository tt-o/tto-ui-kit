# -*- coding: utf-8 -*-
source_suffix = '.rst'
master_doc = 'index'
project = 'TTO UI Kit'
copyright = u'2017, TTO'
version = '1.0'
release = '1.0'
html_theme = 'default'